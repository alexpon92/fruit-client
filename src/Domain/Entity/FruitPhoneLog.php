<?php
declare(strict_types=1);

namespace FruitClient\Domain\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * FruitClient\Domain\Entity\FruitPhoneLog
 *
 * @property int $id
 * @property string $phone
 * @property float $latency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|FruitPhoneLog newModelQuery()
 * @method static Builder|FruitPhoneLog newQuery()
 * @method static Builder|FruitPhoneLog query()
 * @method static Builder|FruitPhoneLog whereCreatedAt($value)
 * @method static Builder|FruitPhoneLog whereId($value)
 * @method static Builder|FruitPhoneLog whereLatency($value)
 * @method static Builder|FruitPhoneLog wherePhone($value)
 * @method static Builder|FruitPhoneLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FruitPhoneLog extends Model
{
    public static function findByPhoneAndDate(string $phone, Carbon $borderDate): ?self
    {
        /** @var self $log */
        $log = self::where('phone', $phone)
            ->where('created_at', '>', $borderDate->toDateTimeString())
            ->first();
        return $log;
    }

    public static function dropByDate(Carbon $borderDate): void
    {
        self::where('created_at', '<', $borderDate->toDateTimeString())
            ->delete();
    }
}