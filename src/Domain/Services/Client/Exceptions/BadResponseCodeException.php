<?php
declare(strict_types=1);

namespace FruitClient\Domain\Services\Client\Exceptions;

class BadResponseCodeException extends \Exception
{

}