<?php
declare(strict_types=1);

namespace FruitClient\Domain\Services\Client\Exceptions;

class MalformedResponseException extends \Exception
{

}