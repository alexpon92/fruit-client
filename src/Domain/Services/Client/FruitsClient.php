<?php
declare(strict_types=1);

namespace FruitClient\Domain\Services\Client;

use FruitClient\Domain\Services\Client\Dto\CheckUserWithTokenResponse;
use FruitClient\Domain\Services\Client\Exceptions\BadResponseCodeException;
use FruitClient\Domain\Services\Client\Exceptions\MalformedResponseException;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class FruitsClient
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $key;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FruitsClient constructor.
     *
     * @param Client          $httpClient
     * @param string          $baseUri
     * @param string          $key
     * @param LoggerInterface $logger
     */
    public function __construct(Client $httpClient, string $baseUri, string $key, LoggerInterface $logger)
    {
        $config                = $httpClient->getConfig();
        $config['http_errors'] = false;
        $config['base_uri']    = $baseUri;

        $this->key        = $key;
        $this->httpClient = new Client($config);
        $this->logger     = $logger;
    }

    /**
     * @param string $phone
     *
     * @return CheckUserWithTokenResponse
     * @throws BadResponseCodeException
     * @throws MalformedResponseException
     */
    public function checkUsersWithToken(string $phone): CheckUserWithTokenResponse
    {
        $this->logger->debug(
            'Making request to fruit to check phone',
            [
                'phone' => $phone
            ]
        );

        $phoneCleared = str_replace('+', '', $phone);

        $start    = microtime(true);
        $response = $this->httpClient->get(
            config('fruit-client.client.methods.check_user_token'),
            [
                'query' => [
                    'phone' => $phoneCleared,
                    'sign'  => md5($phoneCleared . $this->key)
                ]
            ]
        );
        $latency  = round(microtime(true) - $start, 3);

        $content = $response->getBody()->getContents();
        $this->logger->debug(
            'Response from fruits on check phone',
            [
                'response_code' => $response->getStatusCode(),
                'content'       => $content
            ]
        );

        if ($response->getStatusCode() > 299) {
            $this->logger->error(
                'Bad response code on making request to fruits to check phone',
                [
                    'phone'         => $phone,
                    'response_code' => $response->getStatusCode()
                ]
            );
            throw new BadResponseCodeException(
                'Bad response code on making request to fruits to check phone',
                $response->getStatusCode()
            );
        }

        $data = json_decode($content, true);
        if (!isset($data['status'])) {
            $this->logger->error(
                'Malformed response from fruits on phone check',
                [
                    'phone'    => $phone,
                    'response' => $data
                ]
            );
            throw new MalformedResponseException('Malformed response from fruits on phone check');
        }

        return $data['status'] ? CheckUserWithTokenResponse::buildForFound($phone, $latency)
            : CheckUserWithTokenResponse::buildForNotFound($phone, $latency);
    }

    /**
     * @return Client
     */
    public function getHttpClient(): Client
    {
        return $this->httpClient;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

}