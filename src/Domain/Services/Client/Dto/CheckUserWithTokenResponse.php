<?php
declare(strict_types=1);

namespace FruitClient\Domain\Services\Client\Dto;

class CheckUserWithTokenResponse
{
    public const EXISTS     = 'exists';
    public const NOT_EXISTS = 'not_exists';

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $latency;

    /**
     * CheckUserWithTokenResponse constructor.
     *
     * @param string $phone
     * @param string $status
     * @param float  $latency
     */
    public function __construct(string $phone, string $status, float $latency)
    {
        $this->phone   = $phone;
        $this->status  = $status;
        $this->latency = $latency;
    }

    public static function buildForFound(string $phone, float $latency): self
    {
        return new CheckUserWithTokenResponse($phone, self::EXISTS, $latency);
    }

    public static function buildForNotFound(string $phone, float $latency): self
    {
        return new CheckUserWithTokenResponse($phone, self::NOT_EXISTS, $latency);
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return float
     */
    public function getLatency(): float
    {
        return $this->latency;
    }
}