<?php
declare(strict_types=1);

namespace FruitClient\Domain\Services;

use FruitClient\Domain\Entity\FruitPhoneLog;

class CheckPhoneInLog
{
    public function execute(string $phone): bool
    {
        $borderDate = now()->sub(
            new \DateInterval(config('fruit-client.log_actuality_time'))
        );

        $entity = FruitPhoneLog::findByPhoneAndDate($phone, $borderDate);

        return null !== $entity;
    }
}