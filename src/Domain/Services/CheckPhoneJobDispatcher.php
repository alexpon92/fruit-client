<?php
declare(strict_types=1);

namespace FruitClient\Domain\Services;

use FruitClient\Jobs\CheckPhoneJob;

class CheckPhoneJobDispatcher
{
    public function execute(string $phone): void
    {
        $job = new CheckPhoneJob($phone);
        dispatch(
            $job
                ->onQueue((string)config('fruit-client.check_phones_queue.name'))
                ->onConnection((string)config('fruit-client.check_phones_queue.connection'))
        );
    }
}