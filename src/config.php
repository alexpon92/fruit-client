<?php
return [
    'client' => [
        'base_uri' => env('FRUIT_CLIENT_BASE_URI'),
        'key'      => env('KEY'),

        'methods' => [
            'check_user_token' => 'api/checkusertoken'
        ],
    ],

    'check_phones_queue' => [
        'name'       => env('FRUIT_CLIENT_QUEUE_PREFIX', '') . 'check-phone-queue',
        'connection' => 'redis'
    ],

    'log_life_time'      => 'P20D',
    'log_actuality_time' => 'PT24H'
];