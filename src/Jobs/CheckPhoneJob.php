<?php
declare(strict_types=1);

namespace FruitClient\Jobs;

use FruitClient\Domain\Services\Client\Dto\CheckUserWithTokenResponse;
use FruitClient\Domain\Services\Client\FruitsClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use FruitClient\Domain\Entity\FruitPhoneLog;
use Psr\Log\LoggerInterface;

class CheckPhoneJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $phone;

    /**
     * CheckPhoneJob constructor.
     *
     * @param string $phone
     */
    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    public function handle(
        FruitsClient $client,
        LoggerInterface $logger
    ): void {
        $borderDate = now()->sub(
            new \DateInterval(config('fruit-client.log_actuality_time'))
        );

        $entity = FruitPhoneLog::findByPhoneAndDate($this->phone, $borderDate);

        if (!$entity) {
            $response = $client->checkUsersWithToken($this->phone);
            if ($response->getStatus() === CheckUserWithTokenResponse::EXISTS) {
                $entity          = new FruitPhoneLog();
                $entity->phone   = $this->phone;
                $entity->latency = $response->getLatency();
                $entity->save();
            }
        }
    }
}