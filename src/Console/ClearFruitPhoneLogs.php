<?php
declare(strict_types=1);

namespace FruitClient\Console;

use FruitClient\Domain\Entity\FruitPhoneLog;
use Illuminate\Console\Command;

class ClearFruitPhoneLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fruit-client:clear-checked-phones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear checked phones logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $borderDate = now()
            ->sub(new \DateInterval(config('fruit-client.log_life_time')));
        FruitPhoneLog::dropByDate($borderDate);
    }

}