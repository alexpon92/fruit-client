<?php
declare(strict_types=1);

namespace FruitClient;

use FruitClient\Console\ClearFruitPhoneLogs;
use FruitClient\Domain\Services\Client\FruitsClient;
use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class FruitClientServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->publishes(
            [
                __DIR__ . '/config.php' => config_path('fruit-client.php'),
            ],
            'config'
        );

        $this->mergeConfigFrom(
            __DIR__.'/config.php', 'fruit-client'
        );

        $this->app->singleton(
            FruitsClient::class,
            static function ($app) {
                return new FruitsClient(
                    $app->make(Client::class),
                    (string)config('fruit-client.client.base_uri'),
                    (string)config('fruit-client.client.key'),
                    $app->make(LoggerInterface::class)
                );
            }
        );

        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    ClearFruitPhoneLogs::class
                ]
            );
        }

        $this->publishes(
            [
                __DIR__ . '/Database/migrations' => base_path('database/migrations'),
            ],
            'migrations'
        );
    }
}